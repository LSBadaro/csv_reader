package HtmlCreate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteHtml {

    public void writeInHtml(final String endereco) {

        final File file = new File("/home/lucas.badaro/Documents/enderecoEmtel.html");

        if (file.exists()) {
            System.out.println("File already exist");
        } else {
            FileWriter fileWriter = null;
            BufferedWriter bufferedWriter = null;

            try {
                fileWriter = new FileWriter(file);
                bufferedWriter = new BufferedWriter(fileWriter);

                final String htmlPage = "<html><body>";
                bufferedWriter.write(htmlPage);
                bufferedWriter.append(endereco);
                bufferedWriter.append("</body></html>");

                System.out.println("Html page created");
                bufferedWriter.flush();
                fileWriter.close();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    bufferedWriter.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
