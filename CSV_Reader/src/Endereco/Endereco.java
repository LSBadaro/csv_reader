package Endereco;

public class Endereco {
    private String endereco;
    private String enderecoFormatado;
    private String lado;

    public Endereco(final String endereco, final String enderecoFormatado, final String lado) {
        super();
        this.endereco = endereco;
        this.enderecoFormatado = enderecoFormatado;
        this.lado = lado;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(final String endereco) {
        this.endereco = endereco;
    }

    public String getEnderecoFormatado() {
        return enderecoFormatado;
    }

    public void setEnderecoFormatado(final String enderecoFormatado) {
        this.enderecoFormatado = enderecoFormatado;
    }

    public String getLado() {
        return lado;
    }

    public void setLado(final String lado) {
        this.lado = lado;
    }

    @Override
    public String toString() {
        return "Endereco [endereco=" + endereco + ", enderecoFormatado=" + enderecoFormatado + ", lado=" + lado + "]";
    }
}
