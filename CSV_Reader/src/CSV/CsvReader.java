package CSV;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import Barcode.BarcodeGenerate;
import Endereco.Endereco;

public class CsvReader {

    private static final String DELIMITER = ",";
    private static final int ENDERECO_IDX = 0;
    private static final int ENDERECO_FORMATADO_IDX = 1;
    private static final int LADO_IDX = 2;

    public static void readCsv() {

        //@formatter:off
        try (final BufferedReader csv = new BufferedReader(new InputStreamReader(Files.newInputStream(Paths.get("/home/lucas.badaro/Downloads/enderecoEmtel.csv")))); 
             final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(Paths.get("/home/lucas.badaro/Documents/enderecoEmtel.html"),StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)))) {
        //@formatter:on

            final BarcodeGenerate barcode = new BarcodeGenerate();

            csv.readLine();

            final String htmlPage = "<html><body style=' margin-left: 30%; margin-right: 30%;'>";
            out.write(htmlPage);
            
            int count = 0;
            for (String linha; (linha = csv.readLine()) != null;) {
                final String[] tokens = linha.split(DELIMITER);
                if (tokens.length > 0) {
                    if (count == 0) {
                        out.write(
                            "<div style='border: 1px solid black; text-align:center; padding-bottom: 10px; margin-bottom:100%;'>");
                        out.write("<img src='/home/lucas.badaro/Documents/logo.png' style='padding-right: 60%; width:30%'>");
                        final Endereco endereco = new Endereco(tokens[0], tokens[1], tokens[2]);
                        final String imagem = barcode.gerarBarcode(endereco.getEndereco());
                        out.write("<div style='text-align:center; margin-top: -14%; margin-bottom: 25px;'>");
                        out.write("<p>" + endereco.getEnderecoFormatado() + "</p>");
                        out.write("<img src='data:image/x-png; base64," + imagem + "'/>");
                        out.write("</br>");
                        out.write("</div>");
                    } else {
                        if (count < 6) {
                            final Endereco endereco = new Endereco(tokens[0], tokens[1], tokens[2]);
                            final String imagem = barcode.gerarBarcode(endereco.getEndereco());
                            out.write("<div style='text-align:center; margin-bottom: 25px;'>");
                            out.write("<p>" + endereco.getEnderecoFormatado() + "</p>");
                            out.write("<img src='data:image/x-png; base64," + imagem + "'/>");
                            out.write("</br>");
                            out.write("</div>");
                        }
                    }
                    count++;

                    if (count == 6) {
                        out.write("</div>");
                        count = 0;
                    }
                }

            }
            out.write("</body></html>");

        } catch (final Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        }
    }

    public static void main(final String[] args) {
        readCsv();
    }

}
