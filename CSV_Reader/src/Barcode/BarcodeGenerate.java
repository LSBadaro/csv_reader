package Barcode;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Base64;

import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

/**
 * Utilitário para gerar código de barras utilizando biblioteca zxing.
 * 
 * @author lucas.badaro
 *
 */

public class BarcodeGenerate {

    /**
     * Gera um código de barras padrão Code128 e retorna uma String na base64.
     * 
     * @param codigo
     * @return
     * @throws Exception
     */
    public static String gerarBarcode(final String codigo) throws Exception {
        // Create the barcode bean
        final Code128Bean bean = new Code128Bean();
        final int dpi = 150;

        // Configure the barcode generator
        bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi)); // makes the narrow bar
        bean.setFontSize(0); // width exactly one pixel

        bean.doQuietZone(false);

        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            // Set up the canvas provider for monochrome PNG output
            final BitmapCanvasProvider canvas =
                new BitmapCanvasProvider(out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);

            // Generate the barcode
            bean.generateBarcode(canvas, codigo);

            // Signal end of generation
            canvas.finish();

            return new String(Base64.getEncoder().encode(out.toByteArray()));

        } finally {
            out.close();
        }
    }

    // public static void main(final String[] args) throws Exception {
    // Files.write(Paths.get("/tmp", "barcode-128.png"), gerarBarcode("1Cc"));
    // }
}
